package main

import (
	"fmt"
	"log"
	"net/http"
)

func homePage(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello World!"))
}

func main() {
	http.HandleFunc("/", homePage)
	log.Println("Start HTTP server on 3080")
	log.Fatal(http.ListenAndServe(":3080", nil))
	fmt.Println("Listening on localhost:3080...")
}
