package model

type User struct {
	Id       int    `json: "id"`
	Email    string `json: "email"`
	Username string `json: "username"`
	Password string `json: "password"`
	Admin    bool   `json: "admin"`
	Delete   bool   `json: "delete"`
}

type UsersList struct {
	Users []*User
}
